import React from 'react'
import { interpret } from 'xstate/lib/interpreter'
import { string, func, bool } from 'prop-types'

import automatorMachine from '../state/automator'
import Bot from './bot'
import {
  onSpare,
  buildAMator,
  buildAClone,
  onCloneFailed,
  logAction,
} from './_actions.js'
import {
  buildingAMator,
} from './_activities.js'

class Container extends React.Component {
  interpreter

  constructor() {
    super()

    this.state = { progress: 0 }

    const i = interpret(
      automatorMachine
        .withConfig({
          actions: {
            onSpare: onSpare.bind(this),
            buildAMator: buildAMator.bind(this),
            buildAClone: buildAClone.bind(this),
            isBuilt: logAction('isBuilt'),
            onFailed: logAction('onFailed'),
            onCloneFailed,
          },
          activities: {
            buildingAMator: buildingAMator.bind(this)
          }
        })
    )

    i.onTransition( nextState => {
      this.setState({ current: nextState.value })
    })

    this.interpreter = i
  }

  componentDidMount() {
    const { start, name, onReady } = this.props
    if (start) this.interpreter.start()

    onReady({
      name,
      interpreter: this.interpreter
    })
  }

  render() {
    const { name, parent } = this.props
    return (
      <Bot
        name={name}
        parent={parent}
        progress={this.state.progress}
        interpreter={this.interpreter}/>
    )
  }

}

Container.propTypes = {
  name: string.isRequired,
  parent: string,
  onReady: func.isRequired,
  onClone: func.isRequired,
  start: bool,
}

Container.defaultProps = {
  name: 'Bob'
}

export default Container