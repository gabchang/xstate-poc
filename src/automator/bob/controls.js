import React from 'react'
import {
  componentFromProp,
  compose,
  defaultProps,
  mapProps,
  withProps,
} from 'recompose'

const transitionDisabled = ({ on, state, started, disabled }) => {
  if (disabled !== undefined) return disabled
  if (!started) return true
  if (!(on && state)) return false

  if (!state.nextEvents) return true
  return state.nextEvents.indexOf(on) < 0
}

const Button = compose(
  defaultProps({
    component: 'button',
  }),
  withProps(
    ({ children, send, onClick, disabled, state, on, started }) => ({
      children: children || on,
      onClick: onClick || (() => send(on)),
      disabled: transitionDisabled({ on, started, disabled, state }),
    })
  ),
  mapProps(
    ({ send, started, ...rest }) => rest
  )
)(componentFromProp('component'))


export default (props) => {
  const { start, ...rest } = props

  return (
    <div>
      <Button
        onClick={start}
        disabled={false}>
        { rest.started ? 'Restart' : 'Start' }
      </Button>
      <Button
        {...rest}
        on='BUILD'/>
      <Button
        {...rest}
        on='SUCCESS'/>
      <Button
        {...rest}
        on='ERROR'/>
      <Button
        {...rest}
        on='CLONE'/>
    </div>
  )
}
