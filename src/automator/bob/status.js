import React from 'react'
import {
  branch,
  compose,
  defaultProps,
  renderComponent,
} from 'recompose'

const Emoji = ({ type, emoji, className, value }) => (
  <div className="status">
    <span
      role='img'
      className={`status-icon ${className}`}
      arial-label={type}
      >
      { emoji }
    </span>
    { value }
  </div>
)

const Spare = defaultProps({
  emoji: '💤',
})(Emoji)

const Building = defaultProps({
  emoji: '🔨',
  className: 'status-building',
})(Emoji)

const Built = defaultProps({
  emoji: '🤖',
})(Emoji)

const Failed = defaultProps({
  emoji: '☠️',
  className: 'status-dead',
})(Emoji)

const Cloning = defaultProps({
  emoji: '👬',
  className: 'status-cloning',
})(Emoji)


export default compose(
  branch(
    ({ value }) => value === 'failed',
    renderComponent(Failed)
  ),
  branch(
    ({ value }) => value === 'building',
    renderComponent(Building)
  ),
  branch(
    ({ value }) => value === 'built',
    renderComponent(Built)
  ),
  branch(
    ({ value }) => value === 'cloning',
    renderComponent(Cloning)
  ),
)(Spare)

