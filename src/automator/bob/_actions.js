/**
 * ACTIONS => Must be bound to component
 */

export const logAction = (name) => (...args) => console.log(name, args)

export function onSpare() {
  this.setState({ progress: 0 })
}

export function buildAMator() {
  setTimeout(
    () => {
      this.interpreter.send('SUCCESS')
      this.setState({ progress: 0 })
    },
    3000
  )
}

export function buildAClone() {
  const name = prompt("What's the clone name ?")
  if (!(name && name.length > 0)) { 
    this.interpreter.send('ABORT')
    return
  }

  const res = this.props.onClone({ name, parent: this.props.name })

  if (res && res.error) {
    return this.interpreter.send({ type: 'ERROR', error: res.error })
  }

  this.interpreter.send('SUCCESS')
}

export function onCloneFailed(ctx, event) {
  alert(event.error)
}