import React from 'react'
import { string, number, object } from 'prop-types'
import cn from 'classnames'

import Status from './status'
import Controls from './controls'

const Bot = ({ interpreter, name, parent, progress }) => {
  const started = !!interpreter.state
  const failed = (interpreter.state && interpreter.state.value === 'failed')
  const cls = cn(
    'automator',
    {
      'automator-failed': failed
    }
  )

  return (
    <div className={cls}>
      <h2>{ name }</h2>
      <h5>{ parent }</h5>
      <Status value={interpreter.state && interpreter.state.value}/>
      <progress value={progress}/>
      <Controls
        state={interpreter.state}
        start={() => interpreter.start()}
        started={started}
        send={(action) => interpreter.send(action)}/>
    </div>
  )
}

Bot.propTypes = {
  name: string.isRequired,
  interpreter: object.isRequired,
  progress: number,
}

export default Bot