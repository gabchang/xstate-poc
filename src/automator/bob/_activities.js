/**
 * ACTIVITIES => Must be bound to component
 */
export function buildingAMator() {
  let progress = 0
  const progInt = setInterval(
    () => {
      progress++
      this.setState({ 
        progress: progress/10
      })
    },
    200
  )

  return () => clearInterval(progInt)
}