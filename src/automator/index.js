import React from 'react'
import './automator.css'
import Bob from './bob'

const bobIndex = (findName, bobs) => bobs.findIndex( ({ name }) => name === findName )
const getBob = (name, bobs) => {
  return bobs.find( ({ name:bobName }) => bobName === name )
}

const addABob = (bob, bobs) => {
  const { name:cloneName } = bob
  const idx = bobIndex(cloneName, bobs)

  if (idx >= 0) {
    throw new Error(`Bob ${cloneName} already exists !`)
  }

  return [...bobs, bob]
}

const updateABob = (bob, bobs) => {
  const { name } = bob
  const idx = bobIndex(name, bobs)
  const nb = bobs.length

  if (idx < 0) {
    throw new Error(`Bob ${name} does not exist !`)
  }

  if (nb === 1) {
    console.log(`Welcome ${name} ! You're the OB ! (Original Bob)`)
  } else {
    console.log(`Welcome ${name} ! You're at index ${nb-1}`)
  }

  bobs[idx] = {...bobs[idx], ...bob}
  return bobs
}

const deleteABob = (name, bobs) => {
  const idx = bobIndex(name, bobs)

  if (idx < 0) {
    throw new Error(`Bob ${name} does not exist !`)
  }

  if (bobs.length === 1) {
    console.warn('Oh my god ! No more Bob in the universe...')
  }

  return bobs.slice(idx, 1)
}

class Container extends React.Component {

  constructor() {
    super()

    // index 0 is considered the OB (Original Bob)
    // Ready Bob has the shape { name, interpreter, [parent:<String>]}
    this.state = {
      bobs: [
        { name: 'Bob' }
      ]
    }

    // Add a clone to bobNames list
    this.addBob = this.addBob.bind(this)
    // Register a mounted Bob
    this.registerBob = this.registerBob.bind(this)
    // Unregister a mounted Bob
    this.burryBob = this.burryBob.bind(this)
  }

  componentDidMount() {
    const ob = getBob('Bob', this.state.bobs)
    if (ob && ob.interpreter) {
      ob.interpreter.send('BUILD')
    }
  }

  /**
   * Request a new Bob clone
   * @param {Object} bob
   * @param {String} name   - the new clone name
   * @param {String} parent - the bob from which to clone
   */
  addBob(bob) {
    try {
      this.setState({
        bobs: addABob(bob, this.state.bobs)
      })
    } catch (e) {
      return { error: e.message }
    }
  }

  /**
   * Register a mounted bob with its interpreter
   * Should be called AFTER addBob({ name, parent })
   *
   * @param {Object} bob
   * @param {String} bob.name          - [REQUIRED] the Bob's name
   * @param {String} bob.interpreter   - [REQUIRED] the Bob's interpreter
   */
  registerBob(bob) {
    try {
      this.setState({
        bobs: updateABob(bob, this.state.bobs)
      })
    } catch (e) {
      return { error: e.message }
    }
  }

  /**
   * SO long clone-of-bob...
   * @param {*} name
   */
  burryBob(name) {
    try {
      this.setState({
        bobs: deleteABob(name, this.state.bobs)
      })
    } catch (e) {
      return { error: e.message }
    }
  }

  render() {
    const { bobs } = this.state
    return (
      <div>
        <h3>
          Let's build your very own Bobs !
        </h3>
        <div className="automator-list">
          {
            bobs.map(
              ({ interpreter, ...rest }) => (
                <Bob
                  key={rest.name}
                  {...rest}
                  start={true}
                  onClone={this.addBob}
                  onReady={this.registerBob}/>
              )
            )
          }
        </div>
      </div>
    )
  }

}

export default Container