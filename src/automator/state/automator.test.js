import assert from 'assert'
import automatorMachine from './automator'
// import { Machine, actions as machineActions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const say = (msg) => (ctx, event) => (
  console.log(
    event && event.type ? ` ${event.type} =>` : '',
    msg
  )
)

const logTransition = (next) => {
  console.log('▶', next.value)
}

/** TEST DATA  */
const actions = {
  buildMe: say('🐣  Build me please !'),
  buildAMator,
  isBuilt: say('🤖  I am here to do your bidding, Master.'),
  onFailed: say('😨  Oh, sh...'),
}

function buildAMator(ctx, event) {
  say(`🔨  Building ${ctx.name}...`, true)(ctx, event)

  return new Promise( resolve => {
    setTimeout(
      () => {
        event.done()
        resolve()
      },
      500
    )
  })
}

const activities = {
  buildingAMator: () => {
    const interval = setInterval(say('🔜  building...'), 100);
    return () => clearInterval(interval);
  }
}

const makeAutomator = (context) => {
  const i = interpret(
    automatorMachine
      .withConfig({
        actions,
        activities
      })
      .withContext(context)
  )
  i.onTransition(logTransition)
  return i
}

/** /TEST DATA  */

describe('Automator : Initial state', () => {

  const automator = makeAutomator({})
  automator.start()

  test('Initial state', () => {
    assert(
      automator.state.value === 'spare',
      'Automator is initally spare'
      )

    assert(
      automator.send('FAILURE').value === 'spare',
      'Spare parts should not crash'
    )
  })

  automator.stop()
})

describe('Automator : Build-a-mator', () => {

  test('Build-a-mator', (done) => {
    let current
    const automator = makeAutomator({
      name: 'Nono'
    })
    automator.start()

    return new Promise( (resolve, reject) => {
      current = automator.send({
        type: 'BUILD',
        done: () => {
          automator.send('SUCCESS')
          resolve()
        },
      })

      assert(
        current.value === 'building',
        'Start building an automator'
      )
    })
    .then(() => {
      assert(
        automator.state.value === 'built',
        'Automator is built'
      )
      automator.stop()
      done()
    })
  })

  test('Fail-a-mator', () => {
    let current
    const automator = makeAutomator({
      name: 'Super Jaimie'
    })
    automator.start()

    automator.send('BUILD')
    current = automator.send('ERROR')
    assert(
      current.value === 'failed',
      'Automator building failed'
    )
  })

})