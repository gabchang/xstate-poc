import { Machine } from 'xstate'

export const states = {
  spare: {
    onEntry: 'onSpare',
    on: {
      BUILD: 'building',
    }
  },
  building: {
    onEntry: ['buildAMator'],
    activities: ['buildingAMator'],
    on: {
      SUCCESS: 'built',
      ERROR: 'failed',
    }
  },
  built: {
    onEntry: 'isBuilt',
    on: {
      CLONE: 'cloning'
    }
  },
  failed: {
    onEntry: 'onFailed'
  },
  cloning: {
    onEntry: 'buildAClone',
    on: {
      ABORT: 'built',
      SUCCESS: 'built',
      ERROR: 'cloneFailed',
    }
  },
  cloneFailed: {
    onEntry: 'onCloneFailed',
    on: {
      '': 'cloning'
    }
  }
}


export default Machine({
  initial: 'spare',
  states,
})
