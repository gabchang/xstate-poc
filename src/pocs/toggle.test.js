import { Machine } from 'xstate'
import assert from 'assert'


const toggleMachine = Machine({
  initial: 'inactive',
  states: {
    inactive: { on: { TOGGLE: 'active' } },
    active: { on: { TOGGLE: 'inactive' } }
  }
});

describe('Toggle machine', () => {

  test('Initial value', () => {
    assert(
      toggleMachine.initialState.value === 'inactive',
      'Machine is initaly inactive'
    )
  })

  test('Toggle value', () => {
    assert(
      toggleMachine.transition('inactive', 'TOGGLE').value === 'active',
      'Toggle from inactive is active'
    )
    assert(
      toggleMachine.transition('active', 'TOGGLE').value === 'inactive',
      'Toggle from active is inactive'
    )
  })

})
