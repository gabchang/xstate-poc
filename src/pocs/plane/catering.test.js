import assert from 'assert'
import getCatering from './catering'

describe('Catering machine', () => {

  test('Initial', () => {
    const catering = getCatering()
    catering.start()

    assert(
      catering.state.value === 'hungry',
      'catering is initally hungry'
    )

    catering.send('SERVE')
    assert(
      catering.state.value === 'hungry',
      'Can not serve a clandestine !'
    )

    catering.stop()
  })

  test('Serving a non vegan', () => {
    const catering = getCatering({
      class: 'business',
    })
    catering.start()

    catering.send({ type: 'SERVE' })
    assert(
      catering.state.value === 'serving',
      'Can serve a non-clandestine'
    )

    catering.send({ type: 'CHICKEN' })
    assert(
      catering.state.value === 'pollo',
      'Can serve pollo'
    )

    catering.send({ type: 'VEGETABLES' })
    assert(
      catering.state.value === 'spinach_with_pollo',
      'Can serve spinach with pollo'
    )

  })

  test('Serving a vegan', () => {
    const catering = getCatering({
      class: 'organic',
      vegan: true,
    })
    catering.start()

    catering.send({ type: 'SERVE' })
    catering.send({ type: 'CHICKEN' })
    assert(
      catering.state.value === 'serving',
      'Can not serve meat to a vegan'
    )

    catering.send({ type: 'VEGETABLES' })
    assert(
      catering.state.value === 'spinach',
      'Vegan can eat vegetables....'
    )

    catering.send({ type: 'SERVE' })
    catering.send({ type: 'CHICKEN' })
    assert(
      catering.state.value === 'kidding',
      'Vegan asking chicken twice is not a vegan....'
    )

    catering.stop()
  })

})
