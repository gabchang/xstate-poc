import { Machine, actions as mActions } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const { assign, send } = mActions

export const states = {
  key: 'catering',
  "hungry": {
    "on": {
      "SERVE": {
        target: "serving",
        cond: (ctx) => !!ctx.class
      }
    }
  },
  "serving": {
    onEntry: ({ vegan }) => console.log(`🍔  Hungry ${vegan ? 'vegan' : 'flesh eater'} ?`),
    on: {
      CHICKEN: [
        {
          target: 'pollo',
          cond: (ctx) => !ctx.vegan,
          actions: () => console.log('Pollo please !'),
        },
        {
          target: 'meatAlert',
          cond: (ctx) => ctx.vegan,
          actions: () => console.log('Pollo please !'),
        },
      ],
      VEGETABLES: 'spinach',
    }
  },
  "pollo": {
    onEntry: () => console.log('🍗  YUM !'),
    on: {
      VEGETABLES: 'spinach_with_pollo',
    }
  },
  "meatAlert": {
    onEntry: [
      (ctx) => !ctx.warned && console.log('🤢  OMG ...Meat ? Really ??'),
      assign((ctx, event) => ({
        warned: (ctx.warned !== undefined) ? ctx.warned+1 : 0,
      })),
      send('SERVE'),
    ],
    on: {
      SERVE: [
        { target: 'serving', cond: ({ warned }) => !warned },
        { target: 'kidding', cond: ({ warned }) => warned },
      ]
    }
  },
  "kidding": {
    onEntry: () => console.log('🖐  Are you kidding me ? Fake vegan !'),
  },
  "spinach": {
    onEntry: () => console.log('🥦  YUM !'),
    on: {
      SERVE: 'serving',
    }
  },
  "spinach_with_pollo": {
    onEntry: () => console.log('🍗  + 🥦  YUM YUM !'),
  },
}


const cateringMachine = Machine({
  initial: "hungry",
  states,
})

export default (ctx={}) => (
  interpret(
    cateringMachine
      .withContext(ctx)
  )
)
