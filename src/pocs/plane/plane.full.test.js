import { Machine } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'
import assert from 'assert'

import { actions, states as planeStates } from './plane'
import { states as cateringStates } from './catering'

const states = { 
  ...planeStates,
  inflight: {
    ...planeStates.inflight,
    initial: 'hungry',
    states: cateringStates
  }
}

// console.log(states.inflight)

const planeMachine = Machine({
  initial: "idle",
  states
})

const plane = interpret(
  planeMachine
    .withConfig({ actions })
    .withContext({
      class: 'premium'
    })
)

describe('Plane machine', () => {
  test('Plane with onboard catering', () => {
    plane.start()

    assert.equal(
      plane.state.value,
      'idle',
      'Plane is initally idle'
    )

    plane.send('TAKEOFF')
    plane.send('SUCCESS')
    assert.deepEqual(
      plane.state.value,
      { "inflight": "hungry" },
      'Plane is inflight with hungry passengers'
      )

    plane.send('SERVE')
    plane.send('CHICKEN')
    assert.deepEqual(
      plane.state.value,
      { inflight: "pollo" },
      'Passenger chose chicken'
    )

    plane.send('LAND')
    assert.equal(
      plane.state.value,
      'landing',
      'Flight starts landing'
      )

    plane.send('SUCCESS')
    assert.equal(
      plane.state.value,
      'idle',
      'Flight land successfuly'
    )

    plane.stop()
  })
})