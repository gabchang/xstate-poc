import { Machine } from 'xstate'
import { interpret } from 'xstate/lib/interpreter'

const logTime = (msg, date) => {
  console.log(
    msg,
    ' -',
    date ? date.toLocaleTimeString('fr-FR') : ''
  )
}

export const actions = {
  planeTakeoff: (ctx, event) => logTime("🛫", event.time),
  planeInflight: () => console.log("✈️"),
  planeLanding: (ctx, event) => logTime("🛬", event.time),
  safety: () => console.log("🙆‍  Please listen to safety instructions carefuly"),
  fasten: () => console.log("‍🤲  Seatbelts must be fasten during take-off and landing"),
  excuse: () => console.log("😨  Please forgive us for this outrageous technical failure"),
  doa   : (ctx, event) => logTime("💀️  RIP", event.time),
}

export const states = {
  "idle": {
    "onEntry": console.log("🛩  Welcome onboard this non-smoking flight"),
    "on": {
      "TAKEOFF": "takeoff",
    }
  },
  "takeoff": {
    "onEntry": ["safety", "fasten"],
    "on": {
      "SUCCESS": {
        target: "inflight",
        actions: ['planeTakeoff']
      },
      "FAILURE": "error"
    }
  },
  "inflight": {
    onEntry: ['planeInflight'],
    "on": {
      "LAND": "landing",
      "FAILURE": "crashed",
    }
  },
  "landing": {
    "onEntry": ["fasten"],
    "on": {
      "SUCCESS": {
        target: "idle",
        actions: ['planeLanding']
      },
      "FAILURE": "crashed",
    }
  },
  "error": {
    // "onEntry": "excuse",
    "on": {
      "FIX": "idle"
    }
  },
  "crashed": {
    onEntry: "doa"
  }
}


const planeMachine = Machine({
  initial: "idle",
  states,
})

export default interpret(
  planeMachine.withConfig({ actions })
)

