import assert from 'assert'
import plane from './plane'

/** TEST DATA  */

const dt1 = new Date()
const dt2 = new Date()
dt2.setMinutes(dt2.getMinutes() + 15)
const dt3 = new Date()
dt3.setMinutes(dt3.getMinutes() + (3.25*60))

const TIMES = {
  takeoff: dt1,
  crash: dt2,
  landing: dt3,
}

/** /TEST DATA  */

describe('Plane machine', () => {

  // const plane = planeMachine.withConfig({ actions })
  plane.start()

  test('Initial value', () => {
    assert(
      plane.state.value === 'idle',
      'Plane is initally idle'
    )

    assert(
      plane.send('FAILURE').value === 'idle',
      'An idle plane should not crash'
    )
    plane.stop()
  })

  test('Take off', () => {
    let current

    plane.start()

    current = plane.send({ type: 'TAKEOFF' })
    assert(
      current.value === 'takeoff',
      'Take-off'
    )

    current = plane.send({ type: 'SUCCESS', time: TIMES.takeoff })
    assert(
      current.value === 'inflight',
      'Take-off succeeded'
    )

    current = plane.send('TAKEOFF')
    assert(
      current.value === 'inflight',
      'Can not take-off when inflight'
    )

    current = plane.send({ type: 'FAILURE', time: TIMES.crash })
    assert(
      current.value === 'crashed',
      'Inflight failure causes a crash'
    )

    current = plane.send('TAKEOFF')
    assert(
      current.value === 'crashed',
      "Crashed plane can't fly no more"
    )

    plane.stop()

  })

  // test('Inflight', () => {
  //   let current

  //   plane.start()
  //   plane.send('TAKEOFF')
  //   current = plane.send('SUCCESS')

  //   assert(
  //     current.value === 'inflight',
  //     'An idle plane can not land'
  //   )

  //   plane.stop()
  // })

  test('Landing', () => {
    let current

    plane.start()

    current = plane.send('LANDING')
    assert(
      current.value === 'idle',
      'An idle plane can not land'
    )

    plane.send('TAKEOFF')
    plane.send({ type: 'SUCCESS', time: TIMES.takeoff })
    current = plane.send('LAND')
    assert(
      current.value === 'landing',
      'An inflight plane can land'
    )

    current = plane.send({ type: 'SUCCESS', time: TIMES.landing })
    assert(
      current.value === 'idle',
      'A successful landing leads to idle'
    )

    plane.stop()

  })

})
