import React, { Component } from 'react';
import './App.css';
import Automator from './automator'

const Logo = () => (
  <span
    role="img"
    aria-label="logo"
    className="App-logo">
    🤖
  </span>
)

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Logo/>
          <h1>Build-a-Bob</h1>
        </header>
        <Automator/>
      </div>
    );
  }
}

export default App;
