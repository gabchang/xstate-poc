# Communication between statecharts

(since 4.0)

Expressing the entire app's behavior in a single statechart can quickly become complex and unwieldy. It is natural (and encouraged!) to use multiple statecharts that communicate with each other to express complex logic instead. This closely resembles the [actor model](https://en.wikipedia.org/wiki/Actor_model), where each statechart instance is considered an "actor" that can send and receive messages (events) to and from other statechart "actors" and react to them.

With actions, there are a few ways you can do this. For example, you can create your own "supervisor" function that dispatches events to their proper targets:

```js
const pingMachine = Machine({
  id: 'ping',
  initial: 'active',
  states: {
    active: {
      onEntry: sendPing,
      on: {
        PONG: { actions: sendPing }
      }
    }
  }
});

const pongMachine = Machine({
  id: 'pong',
  initial: 'active',
  states: {
    active: {
      onEntry: sendPong,
      on: {
        PING: { actions: sendPong }
      }
    }
  }
});

const interpretedPing = interpret(pingMachine).start();
const interpretedPong = interpret(pongMachine).start();

function sendPing() { sendPingPong('ping'); }
function sendPong() { sendPingPong('pong'); }

function sendPingPong(pingOrPong) {
  setTimeout(() => {
    switch pingOrPong {
      case 'ping':
        return interpretedPong.send('PING');
      case 'pong':
        return interpretedPing.send('PONG');
      default:
        return;
    }
  }, 1000);
}

// => 'ping'
// ...
// => 'pong'
// ...
// => 'ping'
// ...
// => 'pong'
// ...
```

Let's make the classic traffic light example more real-life and model the behavior of two traffic lights at an intersection:

```js
const northLightMachine = Machine({
  id: 'north',
  initial: 'green',
  states: {
    green: {
      after: { 1000: 'yellow' }
    },
    yellow: {
      after: { 1000: 'red' }
    },
    red: {
      onEntry: sendParent('SAFE_SIGNAL'),
      on: { SAFE_SIGNAL: 'green' }
    }
  }
});

const eastLightMachine = Machine({
  id: 'east',
  initial: 'red',
  states: {
    green: {
      after: { 1000: 'yellow' }
    },
    yellow: {
      after: { 1000: 'red' }
    },
    red: {
      onEntry: sendParent('SAFE_SIGNAL'),
      on: { SAFE_SIGNAL: 'green' }
    }
  }
});

const superLightMachine = Machine({
  id: 'supervisor',
  initial: 'active',
  states: {
    active: {
      on: {
        EAST_RED: {
          // tell 'north' machine it is safe to turn green
          actions: send('SAFE_SIGNAL', { to: 'north' })
        },
        NORTH_RED: {
          // tell 'east' machine it is safe to turn green
          actions: send('SAFE_SIGNAL', { to: 'east' })
        }
      }
    }
  }
});

// parent supervisor machine
const interpretedSuperLight = interpret(superLightMachine, {
  children: {
    north: northLightMachine,
    east: eastLightMachine
  }
});
```